//Decoder for wild json formatted text that should be html-decoded
const superMegaAdvancedHtmlEntityDecoder = secretMessage => {
  let textArea = document.createElement("textarea");
  textArea.innerHTML = secretMessage;
  return textArea.value;
};

export default superMegaAdvancedHtmlEntityDecoder;
