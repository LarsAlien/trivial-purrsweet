import VueRouter from "vue-router";
import Home from "./components/home";
import Trivia from "./components/Trivia/Trivia";
import Result from "./components/Result/Result";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/trivia",
    name: "Trivia",
    component: Trivia,
    props: true
  },
  {
    path: "/result",
    name: "Result",
    component: Result,
    props: true
  }
];

const router = new VueRouter({
  routes
});
export default router;
