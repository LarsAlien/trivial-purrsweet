const BASE_URL = "https://opentdb.com/api.php?&type=multiple"; //https://opentdb.com/api.php?&type=multiple&category=15&difficulty=easy&amount=10

const getTrivia = (category, difficulty, amount) => {
  return fetch(
    `${BASE_URL}&category=${category}&difficulty=${difficulty}&amount=${amount}`
  ).then(response => response.json());
};

export default getTrivia;
