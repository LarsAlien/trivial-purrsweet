// Get every category from database
const fetchCategories = () => {
  return fetch(`https://opentdb.com/api_category.php`).then(response =>
    response.json()
  );
};

export default fetchCategories;
